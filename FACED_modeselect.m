function [ FACED_data,pk_loc,noiseLv,Raw2D, order] = FACED_modeselect( est_bg, mode_sep, align_data)
%% This function find the FACED modes automatically
% est_Bg    estimated background of raw data
% mode_sep  user estimated mode separation
% align_data    pre-aligned 2D data
% FACED_data    mode-selected data
% pk_loc    1D location of peaks on the average background

    shifted_bg = est_bg - min(est_bg);
    max_pk = max(shifted_bg);
    [~,pk_loc] = findpeaks(shifted_bg,'MINPEAKDISTANCE',mode_sep-1,'MinPeakProminence',max_pk/16); %[~,pk_loc] = findpeaks(shifted_bg,'MINPEAKDISTANCE',mode_sep-1,'MinPeakProminence',max_pk/32);
    
%     %%%%for wrong-deconvoluted data%%%
a = pk_loc(4:end-3);
clear pk_loc;
pk_loc = a;

order = 0;
   if (pk_loc(2) - pk_loc(1)) < (pk_loc(3) - pk_loc(2))
       order = 1; %DICx --> DICy
   else
       order = 2; %DICy --> DICx
   end
    
    noiseLv = mean(std(align_data(:,[1 end]))); % Assume that the first and last column do not contain data
    
    FACED_data = align_data(:,pk_loc)+align_data(:,pk_loc-1)+align_data(:,pk_loc+1);
    FACED_data= FACED_data /3;
    
    valey = zeros(1,length(pk_loc)-1);
    for kk = 1:(length(pk_loc)-1)
        [~,valey(kk)] = min(est_bg((pk_loc(kk)+2):(pk_loc(kk+1)-2)));
    end
    valey = valey + pk_loc(1:end-1)+1;
    valey = cat(2,valey(1),valey,valey(end));
    valey_data = align_data(:,valey(1:end-1)) + align_data(:,valey(2:end));
    FACED_data = FACED_data - valey_data/2;
    
    Raw2D = align_data;
    
