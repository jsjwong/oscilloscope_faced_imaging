function [ fluo_img,mask,boundary ] = Fluorescence_ImageReconstruct( fluo_data, crop_scale, offset,FM_threshold, mode_loc,targetmode,est_bg, dataCount)
%This function adjust fluorescence raw data into 2D matrix
%

crop_length =  prod(crop_scale);
fluo_data = fluo_data((1:crop_length)+offset+290);
%      fluo_data = fluo_data((1:crop_length)+1500);
fluo_img = reshape(fluo_data,  crop_scale(1), crop_scale(2))';
fluo_img(fluo_img<FM_threshold) = FM_threshold;
fluo_img = fluo_img - min(fluo_img(:))-2;

if nargin == 7
    fluo_edge = floor(crop_scale(1)/targetmode/2);
    startpos = max(1,mode_loc(1)-fluo_edge);
    endpos = min(size(fluo_img,2),mode_loc(end)+fluo_edge);
    fluo_img = fluo_img(:,startpos:endpos);
    excite_bg = interp1(mode_loc,est_bg,(mode_loc(1)-fluo_edge+1):(mode_loc(end)+fluo_edge),'spline');
    if size(excite_bg,2) ~= size(fluo_img,2)
        excite_bg = imresize(excite_bg, [1 size(fluo_img,2)]);
    end
    fluo_img = bsxfun(@rdivide,fluo_img,excite_bg);
end


%%%%% specific for mb231 dataset 52, for masking%%%%%
fluo_img = fluo_img - 2.7656;
fluo_img(fluo_img<3) = 0;
%     fluo_img = fluo_img-4.1436;
%     fluo_img(fluo_img<3) = 0;
fluo_img = imgaussfilt(fluo_img,5);
fluo_img(fluo_img<1) = 0;

fluo_img = circshift(fluo_img,-400,2);
img = imgradient(imgradient(fluo_img)); %6
img_f = imgaussfilt(img, 5);
thres = mean(img_f(:))+0.5*std(img_f(:));
%     thres = mean(img_f(:))+0.7*std(img_f(:));
pre_mask = img_f>thres;
SE = strel('disk',10); %SE = strel('disk',10);
mask_d = imdilate(pre_mask,SE);
mask_e = imerode(mask_d,SE);
%mask_e = imerode(mask_e,SE);
img_fill = imfill(mask_e, 'hole');
img_fill = imerode(img_fill,strel('disk',20));
%mask = imclearborder(img_fill);
mask = bwareaopen(img_fill, 100000);
%     figure; imagesc(fluo_img);colormap copper;
%     figure; imagesc(mask);colormap gray;
mask(fluo_img<1) = 0;
boundary = bwboundaries(mask,'noholes');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%% write the fluo values to a excel file %%%%%%
fluo_intensity = sum(sum(fluo_img));
xlsLabel = ['A'  num2str(dataCount) ':' 'A'  num2str(dataCount)];
xlswrite('fluo.xlsx',fluo_intensity,xlsLabel);

end

