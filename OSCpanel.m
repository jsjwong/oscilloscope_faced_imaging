function [ osc_ax, osc_graph,h_OSC ] = OSCpanel( IO, phyUnit )

    
    % Set up figure;
    screenSize = get(0, 'screensize');
    h_OSC = figure;
    set(h_OSC, 'Position', [0 0 screenSize(4)*1.1 screenSize(4)], 'KeyPressFcn', @command_function);
    spSize = [5 5]; fs = 8;
    
    % Data stream
    osc_ax(1) = subplot(spSize(1), spSize(2), 1:5);
    time_ax = (0:floor(IO.nRecords*IO.NoOfSample-1))*phyUnit.t; 
    osc_graph{1} = plot(osc_ax(1), time_ax, zeros(1, IO.nRecords*IO.NoOfSample),...
                    'b',time_ax, zeros(1, IO.nRecords*IO.NoOfSample),'r');
    xlabel('Time (ns)'); ylabel('ADC codes');
    xlim([1 IO.nRecords*IO.NoOfSample*phyUnit.t]);
    box on; grid on;
    title(osc_ax(1), 'Import data stream', 'FontWeight','bold');
    set(osc_ax(1), 'fontsize', fs);
    
    % Single line scan (background)
    osc_ax(2) = subplot(spSize(1), spSize(2), 6:10);
    time_ax2 = (0:floor(IO.NoOfSample-1))*phyUnit.t; 
    osc_graph{2} = plot(osc_ax(2), time_ax2, zeros(1, IO.NoOfSample),'b');
    axis tight; grid on; box on;
    xlabel('Time (ns)');
    ylabel('ADC codes');
    %xlim([1 IO.NoOfSample*phyUnit.t]);
    osc_graph{2}(1).MarkerEdgeColor = 'r';
    osc_graph{2}(1).MarkerSize = 10;
    set(osc_ax(2), 'fontsize', fs);
    
    % Recontructed image
    osc_ax(3) = subplot(spSize(1), spSize(2), [11 16, 21]);
%     ys = (0:IO.nRecords-1)*phyUnit.y;
    osc_graph{3} = imagesc(zeros(IO.nRecords,IO.NoOfSample),'Parent',osc_ax(3)); 
    axis tight; xticks([]);
    colormap(osc_ax(3), gray(256));
%     ylabel('Length (\mum)'); ylim([0 max(ys)]);
    %title(osc_ax(3), 'Reconstructed image', 'FontWeight','bold');
    set(osc_ax(3), 'fontsize', fs);
        
    % recontructed image DIC or trig
    default_FOVpix = [400 200];
    osc_ax(4) = subplot(spSize(1), spSize(2), [12,17,22]);
    osc_graph{4} = imagesc(zeros(default_FOVpix),'Parent',osc_ax(4));
    axis tight off;
    colormap(osc_ax(4), gray(256));
    set(osc_ax(4), 'xticklabel', [], 'yticklabel', []);
    %title(osc_ax(4), 'Zoom-in reconstructed image', 'FontWeight','bold');
    set(osc_ax(4), 'fontsize', fs);
%    caxis([0.5 1.5]); 
    %caxis([0.75 1.25]); 
%      caxis(osc_ax(4),[0.7 1.3]);
    
    % recontructed image DIC or trig
    osc_ax(5) = subplot(spSize(1), spSize(2), [13,18,23]);
    osc_graph{5} = imagesc(zeros(default_FOVpix),'Parent',osc_ax(5));
    axis tight off;
    colormap(osc_ax(5), gray(256));
    set(osc_ax(5), 'xticklabel', [], 'yticklabel', []);
    %title(osc_ax(4), 'Zoom-in reconstructed image', 'FontWeight','bold');
    set(osc_ax(5), 'fontsize', fs);
%    caxis([0.5 1.5]); 
%     caxis(osc_ax(5),[0.7 1.3]); %caxis([0.6 1.4]);
        
    % QPI
    osc_ax(6) = subplot(spSize(1), spSize(2), [14,19,24]);
    osc_graph{6} = imagesc(zeros(default_FOVpix),'Parent',osc_ax(6));
    axis tight off;
    colormap(osc_ax(6), 'jet');
    set(osc_ax(6), 'xticklabel', [], 'yticklabel', []);
    set(osc_ax(6), 'fontsize', fs);colormap(gca,'jet');
%     caxis([-2 8]);
    xlabel('QPI');
    
    % Fluorescence
    osc_ax(7) = subplot(spSize(1), spSize(2), [15,20,25]);
    osc_graph{7} = imagesc(zeros(default_FOVpix),'Parent',osc_ax(7));
    axis tight off;
    colormap(osc_ax(7), 'copper');
    set(osc_ax(7), 'xticklabel', [], 'yticklabel', []);
    set(osc_ax(7), 'fontsize', fs);
    xlabel('QPI');
end





