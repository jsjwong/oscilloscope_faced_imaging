function [f] = Control_panel
    global Action_command
    global Panel_settings
    %%  Create and then hide the UI as it is being constructed
    f = figure('Visible','off','Position',[160,500,850,285]);
    set(f, 'KeyPressFcn', @command_function);   
    %% Raw data y limits
    uicontrol('Style','text','String','Raw Y_Max','Position',[25,230,70,25]);
    uicontrol('Style','edit','String',Panel_settings.ymax,'Position',[25,210,70,25],'Callback',@ymax_Callback);
    uicontrol('Style','text', 'String','Raw Y_Min','Position',[100,230,70,25]);
    uicontrol('Style','edit','String',Panel_settings.ymin,'Position',[100,210,70,25],'Callback',@ymin_Callback);
    %% ESC
    uicontrol('Style','pushbutton','String','ESC','Position',[300,210,100,50],'Callback',@ESC_Callback);
    %% Image Saving
    displaybutton = uicontrol('Style','togglebutton','String','Image Display','Position',[200,210,100,50],...
        'Callback',@display_Callback);
    savebutton = uicontrol('Style','togglebutton','String','Save','Position',[250,20,100,45],...
        'Callback',@save_Callback);
    
    savegroup = uibuttongroup('Title','Save Mode','Units','pixels','Position',[20,20,200,50],...
                'SelectionChangedFcn',@bselection);
    saveraw =uicontrol(savegroup,'Style','radiobutton','String','Raw','Position',[10,0,100,25]);
    savepic =uicontrol(savegroup,'Style','radiobutton','String','Image','Position',[110,0,100,25]);
    %% Real time settings
    pausebutton = uicontrol('Style','togglebutton','String','Pause','Position',[250,175,70,25],...
        'Callback',@pause_Callback);
    uicontrol('Style','togglebutton','String','Flip','Position',[100,175,70,25],'Callback',@flip_Callback);
    bg = uicontrol('Style','togglebutton','String','Background','Position',[175,175,70,25],'Callback',@bg_Callback);
    uicontrol('Style','togglebutton','String','Trigger','Position',[25,175,70,25],'Callback',@trig_Callback);
    faced = uicontrol('Style','togglebutton','String','FACED','Position',[325,175,70,25],'Callback',@faced_Callback);
    %% FPS Control
    fpsslider = uicontrol('Style','slider','Position',[300,140,70,25],'Callback',@fps_Callback);
    uicontrol('Style','text','String','FPS: 1 Hz','Position',[240,135,60,25]);
    uicontrol('Style','text','String','9 Hz','Position',[370,135,30,25]);
    set(fpsslider,'value',0.3);
    %% EDGE Trigger Panel
    edge_trig = uicontrol('Style','checkbox','Enable','off','Position',[25,140,25,25],...
        'Callback',@edge_trig_Callback);
    uicontrol('Style','text','String','Edge Trig','Position',[40,135,70,25]);
    edge_direction= uicontrol('Style','togglebutton','String','Positive','Position',[110,140,70,25],...
        'Callback',@edge_direction_Callback);
    edge_value = uicontrol('Style','edit','String',Panel_settings.edgetrig.threshold,'Position',[190,140,50,25],...
        'Callback',@edge_value_Callback);
    %% Advanced Trigger Panel
    adv_trig = uicontrol('Style','checkbox','Enable','off','Position',[25,110,25,25],...
        'Callback',@adv_trig_Callback);
    uicontrol('Style','text','String','Adv. Trig','Position',[40,105,70,25]);
    uicontrol('Style','text','String','Bg Threshold','Position',[120,105,70,25]);
    adv_bg = uicontrol('Style','edit','String',Panel_settings.advtrig.bg,'Position',[195,110,50,25],...
        'Callback',@adv_bg_Callback);
    uicontrol('Style','text','String','Trig Threshold','Position',[115,75,80,25]);
    adv_cell = uicontrol('Style','edit','String',Panel_settings.advtrig.cell,'Position',[195,80,50,25],...
        'Callback',@adv_cell_Callback);
    adv_debug = uicontrol('Style','togglebutton','String','Debug','Position',[40,80,70,25],...
        'Callback',@adv_debug_Callback);
    %% FACED QPI
    uicontrol('Style','text','String','FACED delay (pixel)','Position',[420,200,100,50]);
    uicontrol('Style','edit','String',Panel_settings.FACEDdelay,'Position',[550,230,50,25],...
        'Callback',@faceddelay_Callback);
    uicontrol('Style','togglebutton','String','DIC','Position',[620,220,75,40],'Callback',@DIC_Callback);
    uicontrol('Style','togglebutton','String','QPI','Position',[700,220,100,40],'Callback',@QPI_Callback);   
    %% Lecroy Fluorescence
    uicontrol('Style','text','String','Fluorescence Offset (ns)','Position',[410,130,150,50]);
    uicontrol('Style','edit','String',Panel_settings.FMoffset,'Position',[550,160,50,25],...
        'Callback',@FMoffset_Callback);
    uicontrol('Style','togglebutton','String','Fluorescence','Position',[635,158,150,30],'Callback',@FM_Callback);  
    uicontrol('Style','text','String','Fluorescence Threshold','Position',[407,90,150,50]);
    uicontrol('Style','edit','String',Panel_settings.FMthreshold,'Position',[550,120,50,25],...
        'Callback',@FMthreshold_Callback);
    FMvisual = uicontrol('Style','togglebutton','String','Visualize FM Data','Position',[635,118,150,30],...
                'Enable','off','Callback',@FMvisual_Callback); 
    %% Panel Setting
    % Assign the a name to appear in the window title.
    f.Name = 'Control Panel';

    % Move the window to the center of the screen.
    movegui(f,'center')

    % Make the window visible.
    f.Visible = 'on';
    
    %% Callback functions
        function ymax_Callback(source,~)
           ymaxstr = get(source, 'String');
           [ymax_val,tf] = str2num(ymaxstr);
           if tf
               Panel_settings.ymax=ymax_val;
           end      
        end
        function ymin_Callback(source,~)
           yminstr = get(source, 'String');
           [ymin_val,tf] = str2num(yminstr);
           if tf
               Panel_settings.ymin=ymin_val;
           end      
        end
        function ESC_Callback(~,~)
            Action_command{1}  = 1;
        end
        function display_Callback(~,~)
            Panel_settings.imgdisplay = 1-Panel_settings.imgdisplay;
        end
        function save_Callback(source,~)
           savetag = get(source, 'Value');
           Panel_settings.save = 1-Panel_settings.save;
           if savetag
               savebutton.String = 'Stop';
               if Panel_settings.savedata(1)
                    Panel_settings.imgdisplay = 0;
                    displaybutton.Enable = 'Off';
               end
           else
               savebutton.String = 'Save';
               displaybutton.Enable = 'On';
           end
        end
        function bselection(~,~)
            if saveraw.Value
                Panel_settings.savedata(1) = 1;
            else
                Panel_settings.savedata(1) = 0;
            end
            if savepic.Value
                Panel_settings.savedata(2) = 1;
            else
                Panel_settings.savedata(2) = 0;
            end
        end
        function pause_Callback(source,~)
           pausetag = get(source, 'Value');
           if pausetag
               pausebutton.String = 'Resume';
               Action_command{2} = 1;
               fprintf('\nCCTV Paused');
           else
               pausebutton.String = 'Pause';
               Action_command{2} = 0;
               fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
           end
        end
        function flip_Callback(~,~)
             Panel_settings.flip = -1*Panel_settings.flip;
        end
        function bg_Callback(~,~)
             Panel_settings.background = 1-Panel_settings.background;
        end
        function fps_Callback(source,~)
            fpsnum = get(source,'value');
            Panel_settings.fps = 9*fpsnum;
        end
        function trig_Callback(source,~)
            trigtag = get(source, 'Value');
            if trigtag
                edge_trig.Enable = 'on';
                adv_trig.Enable = 'on';
                edge_value_Callback(edge_value);
                adv_bg_Callback(adv_bg);
                adv_cell_Callback(adv_cell);
            else
                edge_trig.Enable = 'off';
                adv_trig.Enable = 'off';
                Panel_settings.edgetrig.enable = 0;
                Panel_settings.advtrig.enable = 0;
            end
        end
        function faced_Callback(~,~)
             Panel_settings.FACED.modeselect = 1-Panel_settings.FACED.modeselect;
        end
        function edge_trig_Callback(source,~)
            edgetrigtag = get(source, 'Value');
            if edgetrigtag
                Panel_settings.edgetrig.enable = 1;
            else
                Panel_settings.edgetrig.enable = 0;
            end
        end
        function edge_direction_Callback(~,~)
             Panel_settings.edgetrig.direction = 1 - Panel_settings.edgetrig.direction;
             if Panel_settings.edgetrig.direction 
                 edge_direction.String = 'Positive';
             else
                 edge_direction.String = 'Negative';
             end
        end
        function edge_value_Callback(source,~)
           edge_str = get(source, 'String');
           [edge_val,tf] = str2num(edge_str);
           if tf
               Panel_settings.edgetrig.threshold = edge_val;
           end  
        end
        function adv_trig_Callback(source,~)
            advtrigtag = get(source, 'Value');
            if advtrigtag
                Panel_settings.advtrig.enable = 1;
            else
                Panel_settings.advtrig.enable = 0;
            end
        end
        function adv_bg_Callback(source,~)
           adv_bg_str = get(source, 'String');
           [adv_bg_val,tf] = str2num(adv_bg_str);
           if tf
               Panel_settings.advtrig.bg = adv_bg_val;
           end  
        end
        function adv_cell_Callback(source,~)
           adv_cell_str = get(source, 'String');
           [adv_cell_val,tf] = str2num(adv_cell_str);
           if tf
               Panel_settings.advtrig.cell = adv_cell_val;
           end  
        end
        function adv_debug_Callback(~,~)
           Panel_settings.advtrig.debug = 1 - Panel_settings.advtrig.debug;
        end
        function faceddelay_Callback(source,~)
           delaystr = get(source, 'String');
           [delay_val,tf] = str2num(delaystr);
           if tf
               Panel_settings.FACEDdelay=delay_val;
           end      
        end
        function DIC_Callback(~,~)
             Panel_settings.QPI.DIC = 1-Panel_settings.QPI.DIC;
             if Panel_settings.QPI.DIC
                Panel_settings.FACED.modeselect = 1;
                faced.Enable = 'Off';
             else
                faced.Enable = 'On';
             end
        end
        function QPI_Callback(~,~)
             Panel_settings.QPI.QPI = 1-Panel_settings.QPI.QPI;
             if Panel_settings.QPI.QPI
                Panel_settings.FACED.modeselect = 1;
                faced.Enable = 'Off';
             else
                faced.Enable = 'On';
             end
        end
        function FM_Callback(source,~)
            FMtag = get(source, 'Value');
            if FMtag
                FMvisual.Enable = 'on';
            else
                FMvisual.Enable = 'off';
            end
             Panel_settings.FMflag = 1-Panel_settings.FMflag;
        end
        function FMoffset_Callback(source,~)
           offsetstr = get(source, 'String');
           [offset_val,tf] = str2num(offsetstr);
           if tf
               Panel_settings.FMoffset=offset_val;
           end      
        end
        function FMthreshold_Callback(source,~)
           thresholdsetstr = get(source, 'String');
           [offset_val,tf] = str2num(thresholdsetstr);
           if tf
               Panel_settings.FMthreshold=offset_val;
           end      
        end
        function FMvisual_Callback(~,~)
             Panel_settings.FMshow = 1-Panel_settings.FMshow;
        end
end