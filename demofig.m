function [] = demofig(dem_fig,img,DIC,phase,fluo_img,scale )

    imgratio = scale.length_x/scale.length_y;
    set(0, 'CurrentFigure', dem_fig)
    if imgratio < 1/4
        imwidth = imgratio;
        imheight = 1;
    else
        imwidth = 1/4;
        imheight = imwidth/imgratio;
    end
        subplot('Position',[0 0 imwidth imheight]);imagesc(DIC(:,:,1));colormap gray;axis off ;
%          caxis([0.7,1.3]);
        subplot('Position',[imwidth 0 imwidth imheight]);imagesc(DIC(:,:,2));colormap gray;axis off ;
%          caxis([0.7,1.3]);
        subplot('Position',[2*imwidth 0 imwidth imheight]);imagesc(phase);colormap(gca,'jet');axis off;
%         lower = 0;
%          if max(phase(:)) < 1
%              lower = -10;
%         end
%         caxis([lower,max(phase(:))]);
        subplot('Position',[3*imwidth 0 imwidth imheight]);imagesc(fluo_img);colormap(gca,'copper');axis off;
end