function [meanfree_img1_final ,meanfree_img2_final ,adv_debug_data, cell_free_bg1,  meanfree_img1_display,  meanfree_img2_display] = FACED_SubtractDIC_BG(img1,img2,exp,zoomFac)


%% Cancel out electrical modulation
rg = 150; %rg=100
sub_y = bsxfun(@minus, img1, mean(img1([1:rg, end-rg:end], :)));
rg = 2; %rg = 5;
ripple_y = mean(sub_y(:, [1:rg+5,end+1-rg:end]),2);
% figure; imagesc(sub_y(:, [1:rg,end+1-rg:end]));
img1 = bsxfun(@minus, img1, ripple_y);

rg = 100; %rg=150
sub_y = bsxfun(@minus, img2, mean(img2([1:rg, end-rg:end], :)));
rg = 2; %rg = 5;
ripple_y = mean(sub_y(:, [1:rg+5,end+1-rg:end]),2);
% figure; imagesc(sub_y(:, [1:rg,end+1-rg:end]));
img2 = bsxfun(@minus, img2, ripple_y);

%     figure; colormap gray;
%     subplot(141); imagesc(DIC_x);  title('DIC_x Before ');
%     subplot(142); imagesc(DICx); title('DIC_x After');
%     subplot(143); imagesc(DIC_y);  title('DIC_y Before ');
%     subplot(144); imagesc(DICy); title('DIC_y After');


% Crop region of low intensity
rng = floor(1:size(img1, 2)*zoomFac);
rng_x = rng+floor(size(img1,2)*(1-zoomFac)/2);
img1 = img1(:, rng_x);
img2 = img2(:, rng_x);
% figure; imagesc(DICx); colormap gray;
% figure; imagesc(DICy); colormap gray;

rg = 150;
img1 = bsxfun(@times, img1, 1./mean(img1([1:rg, end-rg:end], :)));
img2 = bsxfun(@times, img2, 1./mean(img2([1:rg, end-rg:end], :)));

rg = 2; %rg = 5;
meanfree_img1_final = bsxfun(@minus, img1, mean(img1(:, [1:rg+5, end-rg:end]), 2));
meanfree_img2_final = bsxfun(@minus, img2, mean(img2(:, [1:rg+5, end-rg:end]), 2));

%     meanfree_img1_final = movmean(meanfree_img1_final,8,1);
% %     meanfree_img1_final = imresize(meanfree_img1_final,[tempX1 8*tempY1]);
%     meanfree_img2_final = movmean(meanfree_img2_final,8,1);
% %     meanfree_img2_final = imresize(meanfree_img2_final,[tempX1 8*tempY1]);



%By Gwinky for interpolation
meanfree_img1_final = meanfree_img1_final.';
meanfree_img2_final = meanfree_img2_final.';
PeakData_x = NaN((size(meanfree_img1_final,1)-1)*(exp.interp)+1,size(meanfree_img1_final,2));
PeakData_x(1:exp.interp:end,:) = meanfree_img1_final;
InterpPeakData_x = naninterp(PeakData_x,'PCHIP');
meanfree_img1_final = InterpPeakData_x;

PeakData_y = NaN((size(meanfree_img2_final,1)-1)*(exp.interp)+1,size(meanfree_img2_final,2));
PeakData_y(1:exp.interp:end,:) = meanfree_img2_final;
InterpPeakData_y = naninterp(PeakData_y,'PCHIP');
meanfree_img2_final = InterpPeakData_y;

meanfree_img1_final = meanfree_img1_final.';
meanfree_img2_final = meanfree_img2_final.';

%     meanfree_img1_final = imgaussfilt(meanfree_img1_final, 2);
%     meanfree_img2_final = imgaussfilt(meanfree_img2_final, 2);

meanfree_img1_display = meanfree_img1_final;
meanfree_img2_display =  meanfree_img2_final;

meanfree_img1_final = (meanfree_img1_final + 1);
meanfree_img2_final = (meanfree_img2_final + 1)*(-1)+2;

adv_debug_data = meanfree_img1_final;
cell_free_bg1 = meanfree_img1_final;

end