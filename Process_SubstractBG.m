function [ lasercorrect_img,adv_debug_data] = Process_SubstractBG( img, trig_threshold )
%% This function detects cell location, substract background as well as correct for laser fluctuation
    % img           x or y direction image
    % threshold               user defined masking threshold
    % lasercorrect_img      laser corrected image without background
    % adv_debug_data           masking information for debug
    
%% Substract background %%
    %est_bg = mean(img);
    meanfree_img = bsxfun(@minus, img, mean(img(1:10,:)));
   
%% Detect cell location
    noiseLv = mean(std(meanfree_img(:,[1 end])));
    
    outLier = abs(meanfree_img)-trig_threshold*noiseLv;
    outLier_mask = outLier>0;
    outLier_mask_dilate = imdilate(outLier_mask,strel('line',3,0));
    outLier_mask_dilate = imfill(outLier_mask_dilate ,'holes');
    outLier_mask_erode = imerode(outLier_mask_dilate,strel('disk',10));
    outLier_mask_final = bwareaopen(outLier_mask_erode,100);
    outLier_mask_final = imdilate(outLier_mask_final,strel('disk',10));
    
%     medMap = (abs(medfilt2(meanfree_img, [12 3]))- trig_threshold*noiseLv)>0;
%     outLier = medMap;
%     outLier_mask_dilate = imdilate(outLier,strel('rectangle',[12 3]));
%     outLier_mask_dilate = imfill(outLier_mask_dilate,'holes');
%     outLier_mask_erode = imerode(outLier_mask_dilate,strel('rectangle',[12 3]));
%     outLier_mask_erode = bwareaopen(outLier_mask_erode,2000);
%     outLier_mask_final = imdilate(outLier_mask_erode,strel('rectangle',[20 5]));
%     outLier_mask_final = imfill(outLier_mask_final,'holes');
    
    [tempX1,tempY1]=size(outLier);
    adv_debug_data = zeros(tempX1, tempY1, 2);
    adv_debug_data(:,:,1) = outLier;
    adv_debug_data(:,:,2) = outLier_mask_final;
    
%% Correct background fluctuation
    outLier_mask_final = 1-outLier_mask_final;
    outLier_mask_final(outLier_mask_final == 0) = NaN;
    cell_free_bg = mean(outLier_mask_final.*img,'omitnan');
    meanfree_img_final = bsxfun(@rdivide,img,cell_free_bg);
    laser_bg = mean(outLier_mask_final.*meanfree_img_final,2,'omitnan');
    lasercorrect_img = (bsxfun(@rdivide,meanfree_img_final',laser_bg'))';

end