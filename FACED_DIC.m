function [DataX, DataY,mode_loc] = FACED_DIC(imgData,background,pk_loc,Raw2D, order)
% Automatically separate interleaved data into separate X & Y channels

    [~,original_X] = size(imgData);
    image_X = (floor(original_X/2))*2;

if order == 1
    DataX = imgData(:,3:2:image_X);
    DataY = imgData(:,2:2:image_X-2);
    mode_loc = pk_loc(3:2:image_X);
else
    DataY = imgData(:,1:2:image_X);
    DataX = imgData(:,2:2:image_X);
    mode_loc = pk_loc(1:2:image_X);
end

    %Use it when the DICx and DICy are wrongly swapped
    tempx = DataX;
    tempy = DataY;
    DataY = tempx;
    DataX = tempy;

end