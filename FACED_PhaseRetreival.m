function [ phase, mask, boundary] = FACED_PhaseRetreival( datax, datay, scope, scale)
% Input parameters
    % atomconst              Structure containing experimental parameters
    % cell                       Cropped and normalized cells from all 4 channels 
    % scale                     Strucnture containing axis information
% Output parameters
    % phase                    Phase map of image
    
    load('lookup_table.mat');
    tan_na = tan(asin(scope.objNA ));
 %% Phase calculation  %% (Richard)
%      datax = circshift(datax,4,2); %Gwinky's compensation for the "image shift" between DIC X & Y
    portionx = (datax -1)/2;
    portiony = (datay -1)/2;
    portionx(portionx>0.49) = 0.49;
    portiony(portiony>0.49) = 0.49;
    portionx(portionx<-0.49) = -0.49;
    portiony(portiony<-0.49) = -0.49;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%% Gwinky's normalization %%%%%%%%%%%%%%%%%%%%%%%
%     portionx = -0.49 + (portionx-min(min(portionx)))*0.98/(max(max(portionx))-min(min(portionx)));
%     portiony = -0.49 + (portiony-min(min(portiony)))*0.98/(max(max(portiony))-min(min(portiony)));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    delta_x = deltapoints(round(abs(portionx)/0.0001)+1);
    delta_y = deltapoints(round(abs(portiony)/0.0001)+1);
    theta_x = -sign(portionx).*delta_x*tan_na; 
    theta_y = -sign(portiony).*delta_y*tan_na; 
%     theta_x = -sign(portionx).*(atan((abs(portionx)+1).* tan(asin(scope.objNA)))- asin(scope.objNA));
%     theta_y = -sign(portiony).*(atan((abs(portiony)+1).* tan(asin(scope.objNA)))- asin(scope.objNA));
    [H,W,~] = size(portionx);
    lambda = linspace(scope.laserWavelength,scope.laserWavelength,W) * 10^(-9); % Assume that the medium is water
     dphi_x = bsxfun(@times,2*pi./lambda, theta_x); 
    dphi_y = bsxfun(@times,2*pi./lambda, theta_y); 

%     dphi_x = datax; 
%     dphi_y = datay; 
%     [H,W,~] = size(datax);
    
 %% Fourier transform %%
    Gfun = dphi_x + 1i * dphi_y;
    Gfun_ft = fftshift(fft2(Gfun));  
    if mod(W,2) == 1
        kx = ones(H,1)*(-(W-1)/2:1:(W-1)/2); 
    else
        kx = ones(H,1)*(-W/2:1:W/2-1); 
    end
    if mod(H,2) == 1
        ky = ones(W,1)*(-(H-1)/2:1:(H-1)/2); 
    else
        ky = ones(W,1)*(-H/2:1:H/2-1); 
    end
    kx = kx /(scale.length_x*10^(-6));
    ky = ky /(scale.length_y*10^(-6)); 
    k =  kx + 1j*ky';  
    k(abs(k)==0)=Inf+1j*Inf ;
    Phi = Gfun_ft ./k/(2*pi);
    phase = imag(ifft2(ifftshift(Phi)));    
    phase(phase<0) = 0;
    
    
    %%%%%%%%%% Masking by Dickson %%%%%%%%%%%%%5
        %% Source image

 %     phase(phase<-0.5) = -0.5;
%       desize_phase = imresize(phase, [round(size(phase,1)/8) round(size(phase,2)/2)]);
%       desize_phase = padarray(desize_phase,[round(size(phase,1)/8) round(size(phase,2)/2)],0,'both');
%     desize_phase = padarray(phase,[size(phase,1) size(phase,2)],0,'both');
%     desize_phase = padarray(phase,[26 6],'symmetric','both');
%     img = imgradient(imgradient(imgaussfilt(desize_phase,6)));
%     img = img(1+26:end-26,1+6:end-6);
%     img = padarray(img,[20 20],0,'both');
%     ErrType = 0;
    
    % Filtering simply by statistics
    img = imgradient(imgradient(imgaussfilt(phase,3))); %6
    img_f = imgaussfilt(img, 2);
    thres = mean(img_f(:))+0.8*std(img_f(:));
%     thres = mean(img_f(:))+0.7*std(img_f(:));
    pre_mask = img_f>thres;
    SE = strel('disk',5); %SE = strel('disk',10);    
    mask_d = imdilate(pre_mask,SE);
    mask_e = imerode(mask_d,SE);
    img_fill = imfill(mask_e, 'hole');
    mask = imclearborder(img_fill);

    
    % Shape mask
    SE = strel('disk',3);
    mask = imerode(mask,SE);
    mask(phase<0) = 0;
%     mask = imresize(mask, [size(portionx,1)*2 size(portionx,2)*2]);
%     phase_mask = mask(1+20:end-20,1+20:end-20);
%     SE = strel('disk',10);    %SE = strel('disk',6);
%     phase_mask = imerode(phase_mask, SE);
%     phase_mask = bwareaopen(phase_mask, 20000);
%      phase_mask(phase<0) = 0; %phase_mask(phase<0) = 0;
%      phase_mask = bwareaopen(phase_mask, 20000);
%      phase_mask = imresize(phase_mask, [150 150]);
%      phase_mask = imresize(phase_mask, [size(portionx,1) size(portionx,2)]);

     %phase = mask.*phase;
     
     
%    figure; imagesc(phase); colormap jet;
%     figure; imagesc(mask); colormap gray;
    boundary = bwboundaries(mask,'noholes');
%    B = boundary{1};
%    figure; plot(B(:,2),B(:,1),'r','LineWidth',2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%     %Making by Gwinky
%     [Gmag,Gdir] = imgradient(phase);
%     mask = zeros(size(Gmag));
%     mask(50:end-50,1:end) = 1;
%     Gmag = imgaussfilt(Gmag,5);
%     bw = activecontour(Gmag,mask,1500);
%     bw = bwareaopen(bw, 10000);
%     seD = strel('disk',3);
%     bw = imerode(bw,seD);
%     bw = imerode(bw,seD);
%     bw = bwareaopen(bw, 10000);
%     bw = imdilate(bw,strel('disk',5));
%     phase = bw.*phase;
%      phase(phase<0) = 0;
    % phase(phase<-0.5) = -0.5;   % phase(phase<-0.3) = -0.3;
%       phase(phase==0) = -1;
% phase = circshift(phase,983,1);
% phase = imsharpen(phase);
  
end