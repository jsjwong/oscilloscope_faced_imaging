%% Setting of CCTV interface
%% clear mex % Required to be uncomment once if board has been disconnected, power cycled, etc.
close all; clear;
operation_mode = 3;                          %  2 for Lecroy, 3 for Lecroy reprocessing

%% Data import (should check everytime)
IO.savePath = 'E:\THP1_1810\20181018THP1(1)\20181018T1859';              % Directory for data saving
IO.loadPath = 'C:\Users\Kevin Tsia\Desktop\Gwinky\FACED 160720\live mb231\20200716T1315';    % Directory for data retrival
IO.threshold = [0.3, 2, 3, 3];               % Degree of thresholding for image alignment & cell detection
IO.NoOfSample = 125*4;                       % Samples per line. FACED:125*4 = 500, ATOM: 211*4 = 844, or 210*4 = 840
IO.clockSource = 4;                          % 1: int. CLK+int. REF. | 2: int. CLK+ext. REF. | 3: int. CLK+PCIe REF. | 4: ext. CLK
IO.triggerType = 1;                          % 1: int. TRIG | 2: ext. TRIG | 3: SW. TRIG. | 4: Level TRIG.
IO.tirggerSetting = { IO.NoOfSample, {'RISING_EDGE'}, {}, {'RISING_EDGE', 'CH_A', 0}}; % Corresponding trigger setting {trig. Edge, trig. channel, trig. level}.
IO.analogBias = -75;                        % FACED Amp: 1024*4;  ATOM: -80; %Adjustable analog bias in ADQ7 analogue front-end
IO.sign = 1;                                % input sign, set to -1 for inverted input
IO.skip = 1;                                % Sample IO.skip factor (1 = no IO.skip)

%% Experiment setting (should check everytime)
exp.chipCrossSection = [ 60 30 ];       % Microfluidic channel dimension ([width height] in um)
exp.pumpRate = 15;                       % Volumatric pump rate (ml/hr)
exp.FOV = [200, 20, 20];                % Image field-of-view [raw_height, crop_height, crop_width] (in um)
exp.FACEDlinewidth = 30;                % FACED line width (in um)
exp.interp = 6;                       % Image interpolation scale (>0=in pts/um, 0=no interpolation)
exp.FACEDmodes = 50;                    % Mode number for FACED
exp.targetcellnum = 10;               % Target saved cell count

%% Microscope setting (normally unchange)
scope.laserRepTime = 49.975;              % Pulsed laser repetition time (in ns)
scope.laserWavelength = 532;             % Pulsed laser center wavelength (in nm)
scope.objNA = 0.6;                      % Objective lens numerical aperture (-)

%% Lecroy Setting
Lecroy.channel = 'C3';              % Lecroy data channel
Lecroy.maxbuffer = 16000000;        % Maximum buffer size, actual data size depends on OSC setting
Lecroy.xic = 0.0125;                % Assume 80 GS/s sampling rate
Lecroy.fluorescencechannel = 'C2';

%% Run CCTV interface
Process_Interface

