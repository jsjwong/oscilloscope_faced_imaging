function [ align_data, est_bg, offset ] = Process_ImageReconstruct( raw, pts_scan,  nRecords, mode_sep)
%% The function reshape data into 2d matrix with peaks centered
% Input parameters
    % raw                        Raw data get from binary file
    % pts_scan                   Points per scan
    % nRecrods                   Number of scans
    % mode_sep                   Estimated mode separation 
% Output parameters
    % align_data                 Aligned data, no background substraction (+Fluorescent signal if any)
    % est_bg                     Backgound   
    % offset

    %% Pulse offset estimation %%   
    cropped_raw = raw(1:floor(10*pts_scan));
    cropped_bg = mean(reshape(cropped_raw,pts_scan,10),2);
    max_pk = max(cropped_bg-min(cropped_bg));
    [pk_val, pk_loc] = findpeaks(cropped_bg-min(cropped_bg),'MINPEAKDISTANCE',mode_sep-2,'MinPeakProminence',max_pk/16,...
                'MaxPeakWidth',mode_sep*2-2); % Assume that the edges must be between two lowest peaks
    [~,valey_loc] = min(pk_val+[pk_val(end);pk_val(1:end-1)]);
    if valey_loc > 1 
        offset = round((pk_loc(valey_loc)+pk_loc(valey_loc-1))/2);
    else
        offset = 0;
    end
        
    %% Image centering and reconstrution %%
    crop_length =  pts_scan*(nRecords-2);
    raw = raw(floor((1:crop_length)+offset));           
    align_data = reshape(raw,  pts_scan, nRecords-2)'; 
    est_bg = mean(align_data(1:50,:));
    
end



