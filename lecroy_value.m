function [ output_val ] = lecroy_value (DSO, argu_str )
% This function generates double precision output from Lecroy input sting
% DSO         Lecroy object
% input_str   formateed string read from lecroy
% output_val  formateed as double precision value

    invoke(DSO,'WriteString',argu_str,true);
    input_str = invoke(DSO,'ReadString',1000);
    strcolon = strfind(input_str,':');
    input_str = input_str(strcolon+1:end);
    input_str = erase(input_str,' ');
    input_str = erase(input_str,'"');
    output_val = str2double(input_str);

end

