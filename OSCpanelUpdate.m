function [ ] = OSCpanelUpdate(osc_ax,osc_graph,phyUnit,data,linescan,imgData, subfig_data,phase,fluo_img)
        %% Import Settings
        global Panel_settings
        
        %% Raw waveform axis scaling
        if Panel_settings.ymin < Panel_settings.ymax
            osc_ax(1).YLim = [Panel_settings.ymin Panel_settings.ymax];
        end
        
        %% Raw waveform
        osc_graph{1}(1).YData = data; 
        osc_graph{1}(1).XData = (0:(length(data)-1))*phyUnit.t; 
        if Panel_settings.FMshow
           osc_graph{1}(2).YData = Panel_settings.FMthreshold*ones(1, length(data)); 
           osc_graph{1}(2).XData = (0:(length(data)-1))*phyUnit.t; 
        elseif Panel_settings.edgetrig.enable
           osc_graph{1}(2).YData = Panel_settings.edgetrig.threshold*ones(1, length(data)); 
           osc_graph{1}(2).XData = (0:(length(data)-1))*phyUnit.t; 
        end
        
        %% Spectrum
        osc_graph{2}(1).YData = linescan;
        osc_graph{2}(1).XData = (0:(length(linescan)-1))*phyUnit.t; 
        if Panel_settings.FACED.modeselect
            osc_graph{2}(1).MarkerIndices = Panel_settings.FACED_Pks;
            osc_graph{2}(1).Marker = 'x';
        else
            osc_graph{2}(1).Marker = 'none';
        end

        %% Reconstructed image 
        osc_graph{3}.CData = imgData; 
        
        %% Debug mode of advanced trigger or DIC
        osc_graph{4}.CData = subfig_data(:,:,1); 
%         caxis(osc_ax(4),[0.5 1.5]);
%          caxis(osc_ax(4),[0.7 1.3]);
        osc_graph{5}.CData = subfig_data(:,:,2);  
%         caxis(osc_ax(5),[0.5 1.5]);
%         caxis(osc_ax(5),[0.7 1.3]);
        
        %% QPI
        osc_graph{6}.CData = phase;
%          lower = 0;
%          if max(phase(:)) < 1
%              lower = -10;
%          end
%          caxis(osc_ax(6),[lower max(phase(:))]);
        
        %% Fluorescence
        osc_graph{7}.CData = fluo_img;
        %caxis(osc_ax(7),[min(fluo_img(:)) 0.22]);
end

