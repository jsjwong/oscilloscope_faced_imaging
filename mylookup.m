funcstr = '0.5-acos(x)/pi+x.*sqrt(1-x.^2)/pi'; % Define the sine function
xmin = 0; % Set the minimum input of interest
xmax = 1; % Set the maximum input of interest
xdt = ufix(16); % Set the x data type
xscale = 2^-16; % Set the x data scaling
ydt = ufix(16); % Set the y data type
yscale = 2^-14; % Set the y data scaling
rndmeth = 'Floor'; % Set the rounding method
errmax = 2^-15; % Set the maximum allowed error
nptsmax = 21; % Specify the maximum number of points
[xdata, ydata, errworst] = fixpt_look1_func_approx(funcstr, ...
xmin,xmax,xdt,xscale,ydt,yscale,rndmeth,errmax,[]);
fixpt_look1_func_plot(xdata,ydata,funcstr,xmin,xmax,xdt, ...
xscale,ydt,yscale,rndmeth);

ydata_new = 0:0.0001:0.5;
xdata_new = interp1(ydata,xdata,ydata_new);
portionpoints = ydata_new;
deltapoints = xdata_new;
save('lookup_table','portionpoints','deltax');