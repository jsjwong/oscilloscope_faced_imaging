function [ width, height, final_sampling, varargout] = FACED_resample( data, pts_scan, xic )
%% This function deal with the problem of Non-integer number within one period By digitally resampling the data
    % Input arguments
    % data                      raw data
    % pts_scan                  estimated numer of samples per scan
    % xic                       sampling interval
    % Ouput arguments
    % new_data                  resampled data
    % width                     new number of samples per scan
    % height                    number of scans
    % final_sampling            calculated sampling interval
    
%% Initialization
    channel_num = size(data,1); % Detect whether there is multiple channel of data
    raw = data(1,:);
    total_time = xic * (length(raw)-1);
   
%% Detecting pulse repition rate
    data_corr = xcorr(raw,round((length(raw ))/10),'unbiased');
    [~,peakloc] = findpeaks(data_corr(1:length(data_corr)),'MinPeakDistance',pts_scan-7);
    peakdiff = diff(peakloc);
    sort_peak = peakdiff > (pts_scan - 2);
    peakdiff = peakdiff(sort_peak ~=0);
    peakdiff(peakdiff >= (pts_scan + 2)) = ones(sum((peakdiff >= (pts_scan + 2))),1) * (pts_scan);
    raw_period = mean(peakdiff);                    %% Number of points within a period
    width = round(raw_period);
    new_sampling = raw_period * xic / width;
    height = floor(length(raw)/width);
    
%% Preset argument before optimization
    ori_scale = (0:(length(raw)-1))*xic;
    [~,offset] = max(raw(1:width));
    
%% Optimization 
    range = 4e-8;
    opt_order = 5e-9;
    opts = optimset('TolX', opt_order); 
    fobj = @(sampling) optimization(raw,sampling,ori_scale, total_time,width, height, offset); 
    final_sampling = fminbnd(fobj, new_sampling - range, new_sampling + range, opts); 
    
    %rep_time = new_sampling * width*1e9;         %% Repitition time of laser 
    scale = 0:final_sampling:total_time;
    if channel_num == 1
        varargout{1} = interp1(ori_scale,data,scale,'pchip');
    else
        varargout{1} = interp1(ori_scale,data(1,:),scale,'pchip');
        fluo_dwnsample = round(length(data(1,:))/length(data(2,:)));
        varargout{2} = interp1((0:(length(data(2,:))-1))*fluo_dwnsample*xic,data(2,:),scale,'pchip');
    end
    height = floor(length(varargout{1}) / width);
    
end

function [ std_img ] = optimization(data, sampling, ori_scale, total_time, width, height,offset)
    
    scale = 0:sampling:total_time;
    new_data = interp1(ori_scale,data,scale);

    img = (reshape(new_data(((1:width*(height-10))+floor(width/2)+round(width/2) - offset)), width, height-10));
    std_img = sum(var(img,0,2));
    
end


