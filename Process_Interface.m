%% -------------------Set up environment--------------------------------------%%
if operation_mode == 2
    %% Lecroy Initialize %%
    % MAKE CONNECTION AND READ DATA FROM SCOPE %
    figure();
    DSO = actxcontrol('LeCroy.ActiveDSOCtrl.1'); % Load ActiveDSO control
    invoke(DSO,'MakeConnection','IP:127.0.0.1'); % Substitute your choice of IP address here
    invoke(DSO,'WriteString','*IDN?',true); % Query the scope name and model number
    deviceID=invoke(DSO,'ReadString',1000);% Read back the scope ID to verify connection
    Lecroy.xic = lecroy_value(DSO, strcat(Lecroy.channel,':INSPECT? "HORIZ_INTERVAL",FLOAT')) * 10^9;   % Sampling interval (in ns)
    Lecroy.nPoints = lecroy_value(DSO, strcat(Lecroy.channel,':INSPECT? "WAVE_ARRAY_1",FLOAT'));
    Lecroy.yorig = lecroy_value(DSO, strcat(Lecroy.channel,':INSPECT? "VERTICAL_OFFSET",FLOAT'));
    Lecroy.yic = lecroy_value(DSO, strcat(Lecroy.channel,':INSPECT? "VERTICAL_GAIN",FLOAT'));
    Lecroy.fluorescence.nPoints = lecroy_value(DSO, strcat(Lecroy.fluorescencechannel,':INSPECT? "WAVE_ARRAY_1",FLOAT'));
    Lecroy.fluorescence.yorig = lecroy_value(DSO, strcat(Lecroy.fluorescencechannel,':INSPECT? "VERTICAL_OFFSET",FLOAT'));
    Lecroy.fluorescence.yic = lecroy_value(DSO, strcat(Lecroy.fluorescencechannel,':INSPECT? "VERTICAL_GAIN",FLOAT'));
end
[ IO, phyUnit, Lecroy ] = preset_environment(IO, exp, scope, Lecroy, operation_mode);

%% -------------------Set up user control callbacks---------------------------%%
global Action_command
global Panel_settings
Action_command = {0, 0};

Panel_settings.ymax = 200;
Panel_settings.ymin = 0;
Panel_settings.background = 0;
Panel_settings.flip = IO.sign;
Panel_settings.save = 0;
Panel_settings.savedata = [1 0];    % data to be saved, [raw waveform, img, QPI]
Panel_settings.fps = 3;
Panel_settings.imgdisplay = 1;

Panel_settings.edgetrig.enable = 0;
Panel_settings.edgetrig.direction = 1;  % 1 for positive trigger, 0 for negative
Panel_settings.edgetrig.threshold = 0;
Panel_settings.advtrig.enable = 0;
Panel_settings.advtrig.bg = 5;
Panel_settings.advtrig.cell = 10;
Panel_settings.advtrig.debug = 0;

mode_septime = 0.5; % mode separation time (in ns)
Panel_settings.FACEDdelay = floor(mode_septime/phyUnit.t/5*4); % pixels between two adjacent modes for FACED
Panel_settings.FACED_Pks = 1;
Panel_settings.FACED.modeselect = 1;
Panel_settings.QPI.DIC = 0;
Panel_settings.QPI.QPI = 0;
Panel_settings.FMflag = 0;
Panel_settings.FMoffset = 0;
Panel_settings.FMthreshold = 0;
Panel_settings.FMshow = 0;

%% -------------------Initialise parameters for data read/visualisation ------%%

% Data Retrieval parameters with "interface_ADQ('getdata',..."
NumberOfRecords = IO.nRecords;
target_bytes_per_sample = 2;
StartRecordNumber = 0;
ChannelsMask = 15;
StartSample = 0;
nSamples = IO.NoOfSample;
target_buffer_size = IO.nRecords*IO.NoOfSample;

% Timing of framerate
t_prev = 0;
FPS = 1;
bufTime = 0;
fprintf('Frame rate (fps): %.2f | Buffer time %.2f seconds', FPS, bufTime);

% Count of captured images
CaptureCount = 0;
dataCount = 0;
subfig_data = zeros(400,400,2);
phase = zeros(400,400);
fluo_img = zeros(400,400);

% Set up display pannel
[osc_ax, osc_graph,h_OSC] = OSCpanel(IO, phyUnit);
control_figure = Control_panel;
screensize = get( groot, 'Screensize' );
dem_fig = figure('position',[0 0 round(screensize(4)/2) round(screensize(4)/2)]);
dem_fig.Visible = 'Off';

% Save path creation
if operation_mode == 0 || operation_mode == 2
    IO.savePath = strcat(IO.savePath,'\',datestr(now,'yyyymmddTHHMM')); %Set up Savepath
    if ~exist(IO.savePath, 'dir')
        mkdir(IO.savePath);
    end
    fileName = strcat(IO.savePath,'\','saveItem.h5');
    if operation_mode == 2
        h5create(fileName,'/data',[Inf Lecroy.nPoints],'ChunkSize',[1 Lecroy.nPoints]);
        h5create(fileName,'/fluorescence',[Inf Lecroy.fluorescence.nPoints],'ChunkSize',[1 Lecroy.fluorescence.nPoints]);
    end
end

%% -------------------Image Frame Data Acquisition Loop-----------------------%%
tic
while (~Action_command{1}) % Not ESC
    
    %% Update panel
    drawnow
    if (Action_command{2}) % Pause
        pause(0.2);
        tic
        t_prev = 0;
        continue;
    end
    
    %% Data retrieval
    if operation_mode == 2
        %% Lecroy Digitization
        % Get Wavefrom Data - 16 Mpts is chosen as the arbitrary maximum file transfer file size (substitute your own max value to use)
        invoke(DSO,'WriteString','ARM;WAIT',true);
        data=invoke(DSO,'GetByteWaveform',Lecroy.channel,Lecroy.maxbuffer,0);
        % ActiveDSO transfers single precision matrix.  Need to convert to double to plot in Matlab.
        data=double(data);
        if Panel_settings.FMflag
            fluo_data=invoke(DSO,'GetByteWaveform',Lecroy.fluorescencechannel,Lecroy.maxbuffer,0);
            fluo_data=double(fluo_data);
        end
    elseif operation_mode == 3
        %% Local Lecroy DATA DEBUG
        dataID = rem(dataCount,Lecroy.CaptureCount)+1;
        data = h5read(strcat(IO.loadPath,'\saveItem.h5'),'/data',[dataID 1],[1 Lecroy.nPoints]);
        if Panel_settings.FMflag
            fluo_data = h5read(strcat(IO.loadPath,'\saveItem.h5'),'/fluorescence',[dataID 1],[1 Lecroy.fluorescence.nPoints]);
        end
        dataCount = dataCount + 1;
    end
    
    %% Display-mode dependent processing
    if Panel_settings.imgdisplay
        %% 1D data stream -> 2D image,Background substraction %%
        if operation_mode == 2 || operation_mode == 3
            if ~Panel_settings.FMflag
                [ IO.NoOfSample, IO.nRecords, ~, data] = FACED_resample( data, IO.NoOfSample, Lecroy.xic);
            else
                [ IO.NoOfSample, IO.nRecords, ~, data, fluo_data] = FACED_resample( [data;fluo_data],...
                    IO.NoOfSample, Lecroy.xic);
            end
        end
        
        [ imgData, est_bg, offset] = Process_ImageReconstruct( Panel_settings.flip*data,...
            IO.NoOfSample, IO.nRecords,Panel_settings.FACEDdelay);
        
        %% FACED mode detection %%
        if Panel_settings.FACED.modeselect
            [imgData,Panel_settings.FACED_Pks,noiseLv,Raw2D, order] = FACED_modeselect( est_bg, Panel_settings.FACEDdelay, imgData);
        end
        
        %% FACED DIC %%
        if Panel_settings.QPI.QPI || Panel_settings.QPI.DIC
            [X_Data, Y_Data,mode_loc] = FACED_DIC(imgData,est_bg(Panel_settings.FACED_Pks),Panel_settings.FACED_Pks,Raw2D, order);
            zoomFac = 1;
            [lasercorrect_imgX,lasercorrect_imgY, adv_debug_data, FACED_bg, meanfree_img1, meanfree_img2] = FACED_SubtractDIC_BG(X_Data, Y_Data,...
                exp,zoomFac);
        else
            [ ~,adv_debug_data ] = Process_SubstractBG( imgData, Panel_settings.advtrig.bg );
        end
        
        %% FACED fluorescence %%
        if Panel_settings.FMflag
            fluo_offset = round(offset+rem(Panel_settings.FMoffset,scope.laserRepTime)/scope.laserRepTime*IO.NoOfSample);
            if Panel_settings.FACED.modeselect
                [ fluo_img,fluo_mask,fluo_boundary ] = Fluorescence_ImageReconstruct( fluo_data, [IO.NoOfSample IO.nRecords-2],fluo_offset,...
                    Panel_settings.FMthreshold,mode_loc,exp.FACEDmodes,est_bg(mode_loc),dataCount); %% Changed to 'FACED_bg' from 'est_bg(mode-loc)'
                %Fluorescence_ImageReconstruct( fluo_data, [IO.NoOfSample IO.nRecords-2],fluo_offset,...
                %Panel_settings.FMthreshold,mode_loc,exp.FACEDmodes,est_bg(mode_loc),dataCount,outLier_mask_final);
            else
                [ fluo_img,fluo_mask,fluo_boundary ] = Fluorescence_ImageReconstruct( fluo_data, [IO.NoOfSample IO.nRecords-2],fluo_offset,dataCount);
            end
        end
        
        %% Trigger mode %%
        if Panel_settings.edgetrig.enable
            if Panel_settings.edgetrig.direction == 1
                if max(data) < Panel_settings.edgetrig.threshold
                    continue
                end
            else
                if min(data) > Panel_settings.edgetrig.threshold
                    continue
                end
            end
        end
        if Panel_settings.advtrig.enable
            outLier_mask = adv_debug_data(:,:,2);
            outLier = adv_debug_data(:,:,1).*adv_debug_data(:,:,2);
            if sum(outLier_mask(:)) > 0
                outLier_sum = sum(outLier(:))/sum(outLier_mask(:));
                if outLier_sum <= Panel_settings.advtrig.cell
                    continue
                end
            else
                continue
            end
        end
        
        if Panel_settings.QPI.DIC && ~Panel_settings.advtrig.debug
            [tempX1,tempY1]=size(lasercorrect_imgX);
            DIC_data = zeros(tempX1, tempY1, 2);
            %             DIC_data(:,:,1) = meanfree_img1;
            %             DIC_data(:,:,2) = meanfree_img2;
            %
            DIC_data(:,:,1) = lasercorrect_imgX;
            DIC_data(:,:,2) = lasercorrect_imgY;
            subfig_data = DIC_data;
        elseif  Panel_settings.advtrig.debug
            subfig_data = adv_debug_data;
        else
            subfig_data = subfig_data *0;
        end
        
        %% Phase retrival %%
        if Panel_settings.QPI.QPI
            scale.length_y = size(X_Data,1)*phyUnit.y;
            scale.length_x = (exp.FACEDlinewidth/exp.FACEDmodes)*size(X_Data,2)*zoomFac;
            [phase, phase_mask, boundary] = FACED_PhaseRetreival( lasercorrect_imgX, lasercorrect_imgY, scope, scale); %phase = FACED_PhaseRetreival( lasercorrect_imgX, lasercorrect_imgY, scope, scale);
        end
        
        %% Update OSC panel content %%
        if ~Panel_settings.background
            display_img = imgData;
            %             display_img = flip(imgData,1);
        else
            display_img = bsxfun(@minus,imgData, mean(imgData));
            %             display_img = flip(bsxfun(@minus,imgData, mean(imgData)),1);
        end
        if ~Panel_settings.FMshow
            paneldata = data((1:(IO.nRecords-2)*IO.NoOfSample)+offset);
        else
            paneldata = fluo_data((1:(IO.nRecords-2)*IO.NoOfSample)+fluo_offset);
        end
        OSCpanelUpdate(osc_ax, osc_graph, phyUnit, paneldata, est_bg, display_img, subfig_data, phase, fluo_img);
        
        %% Frame rate control %%
        t_end=toc;
        actual_FPS = Panel_settings.fps;
        bufTime = 1/Panel_settings.fps-(t_end-t_prev);
        if bufTime>4
            bufTime = 4-(t_end-t_prev); Panel_settings.fps = 1/4;
        elseif bufTime < 0
            bufTime = 0; actual_FPS = 1/(t_end-t_prev);
        end
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b');
        fprintf('Frame rate (fps): %.2f | Buffer time %.2f seconds', actual_FPS , bufTime);
        pause(bufTime);
        t_prev = toc;
        
    end
    
    %% Save data
    if any(Panel_settings.savedata) && Panel_settings.save
        CaptureCount = CaptureCount+1;
        if Panel_settings.savedata(1)
            h5write(fileName,'/data',data,[CaptureCount 1],[1 Lecroy.nPoints]);
            if Panel_settings.FMflag
                h5write(fileName,'/fluorescence',fluo_data,[CaptureCount 1],[1 Lecroy.nPoints]);
            end
        elseif Panel_settings.savedata(2)
            demofig(dem_fig,display_img,subfig_data,phase,fluo_img,scale )
            if ~exist(strcat(IO.loadPath,'\image'), 'dir')
                mkdir(strcat(IO.loadPath,'\image'));
            end
            saveas(dem_fig,strcat(IO.loadPath,'\image\',num2str(dataID), '.tiff'));
            if dataCount>Lecroy.CaptureCount
                Action_command{1} = 1;
            end
        end
    end
    
end

%% -------------------Close CCTV----------------------------------------------%%
fprintf('\nCCTV Stopped')
if operation_mode == 0
    interface_ADQ('disarmtrigger', [] ,FPGA.boardID);
    [~, ~, status, ~] = interface_ADQ('multirecordclose', [], FPGA.boardID);
elseif operation_mode == 2
    invoke(DSO,'Disconnect'); % disconnect from scope
end

%% -------------------Remove save folder if no data is saved------------------%%
listing = dir(IO.savePath);
if operation_mode == 0 || operation_mode == 2
    if listing(3).bytes < 10000  % Indicating no data
        rmdir(IO.savePath,'s');  % Delete empty savepath
    else
        save([IO.savePath, '\setting.mat'], 'IO', 'exp', 'scope', 'FPGA', 'Lecroy');
        h5writeatt(fileName,'/','CaptureCount',CaptureCount);
    end
end
fprintf('\n*** ADQ7 disarmed and multi-record acquisition closed successfully. ***\n')
close all
