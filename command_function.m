function command_function(~, event)

    % Variable carring command information
    global Action_command
    
    % Action
    if (strcmp(event.Key, 'escape'))
        Action_command{1}  = 1;
    elseif (event.Key == 'p')
        if (Action_command{2})
            Action_command{2} = 0;
            fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
        else
            Action_command{2} = 1;
            fprintf('\nCCTV Paused')
        end
    end
    
end