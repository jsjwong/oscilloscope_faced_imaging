function close_acquisition_fn(~, ~)
    global Action_command
    delete(gcf);
    if (Action_command{1} ~= 1)
        Action_command{1}  = 1;
    end
end

