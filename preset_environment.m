function [ IO, phyUnit, Lecroy ] = preset_environment(IO, exp, scope, Lecroy, operation_mode)

    phyUnit.x = exp.FACEDlinewidth/exp.FACEDmodes; % space increament (spectrum encode, in um)
    linFlowRate = exp.pumpRate/3.6e9/(prod(exp.chipCrossSection)*10^-12);
    phyUnit.y = (scope.laserRepTime*1e-9*1e6)*linFlowRate; % space increament (scan, in um)
    
    if  operation_mode == 2
        phyUnit.t = Lecroy.xic;
        IO.NoOfSample = floor(scope.laserRepTime/Lecroy.xic);
        IO.nRecords = floor(Lecroy.nPoints/IO.NoOfSample);
    elseif operation_mode == 3
        phyUnit.t = Lecroy.xic;
        fileinfo = h5info(strcat(IO.loadPath,'\saveItem.h5'),'/data');
        Lecroy.nPoints = prod(fileinfo.ChunkSize);
        fluoinfo = h5info(strcat(IO.loadPath,'\saveItem.h5'),'/fluorescence');
        Lecroy.fluorescence.nPoints = prod(fluoinfo.ChunkSize);
        IO.NoOfSample = floor(scope.laserRepTime/Lecroy.xic);
        IO.nRecords = floor(Lecroy.nPoints/IO.NoOfSample);
        Lecroy.CaptureCount = 5000;
        %Lecroy.CaptureCount = h5readatt(strcat(IO.loadPath,'\saveItem.h5'),'/','CaptureCount');
    end
    
    IO.nRecords = IO.nRecords + 1; %add one extra line to accommodate samples dropped to achieve start alignment

end

